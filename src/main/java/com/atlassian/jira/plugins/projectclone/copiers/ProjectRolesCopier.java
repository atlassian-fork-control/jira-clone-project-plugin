/*
 * Copyright © 2013 - 2015 Atlassian Pty Ltd. Licensed under the Apache License, Version
 *  2.0 (the "License"); you may not use this file except in compliance with the License. You
 *  may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless
 *  required by applicable law or agreed to in writing, software distributed under the License is
 *  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *  either express or implied. See the License for the specific language governing permissions
 *  and limitations under the License.
 */

package com.atlassian.jira.plugins.projectclone.copiers;

import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.plugins.projectclone.extensions.ProjectSettingsCopier;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.util.ErrorCollection;
import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class ProjectRolesCopier implements ProjectSettingsCopier {
	@Autowired
	protected ProjectRoleManager projectRoleManager;
	@Autowired
	protected ProjectRoleService projectRoleService;

	@Override
	public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
		projectRoleManager.removeAllRoleActorsByProject(newProject);
		Collection<ProjectRole> roles = projectRoleManager.getProjectRoles(); // roles are globals, actors (users/groups) are project specific
		for(ProjectRole role : roles) {
			ProjectRoleActors projectActors = projectRoleManager.getProjectRoleActors(role, project);
			if (projectActors != null) {
				Set<RoleActor> realActors = projectActors.getRoleActors();
				if (realActors != null) {
					for (RoleActor actor : realActors) {
						projectRoleService.addActorsToProjectRole(ImmutableList.<String>of(actor.getParameter()), role, newProject,
								actor.getType(), errorCollection);
					}
				}
			}
		}
	}
}
