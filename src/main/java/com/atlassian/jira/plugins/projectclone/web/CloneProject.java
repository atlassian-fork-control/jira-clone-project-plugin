/*
 * Copyright © 2013 - 2015 Atlassian Pty Ltd. Licensed under the Apache License, Version
 *  2.0 (the "License"); you may not use this file except in compliance with the License. You
 *  may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless
 *  required by applicable law or agreed to in writing, software distributed under the License is
 *  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *  either express or implied. See the License for the specific language governing permissions
 *  and limitations under the License.
 */

package com.atlassian.jira.plugins.projectclone.web;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.user.search.UserPickerSearchService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.plugins.projectclone.extensions.ProjectSettingsCopierModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.action.project.AddProject;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableSortedSet;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Comparator;
import java.util.List;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class CloneProject extends AddProject {
	@Autowired
	protected PluginAccessor pluginAccessor;

	@Autowired
	protected WebResourceManager webResourceManager;

    @Autowired
    protected WorkflowSchemeManager workflowSchemeManager;

	protected Project newProject;

	public CloneProject(ProjectService projectService, AvatarService avatarService, AvatarManager avatarManager,
			PermissionSchemeManager permissionSchemeManager, UserManager userManager,
			UserPickerSearchService userPickerSearchService, FeatureManager featureManager) {
		super(projectService, avatarService, avatarManager, permissionSchemeManager, userManager,
				userPickerSearchService,
				featureManager);
	}

	@Override
	protected void setProjectReference(Project project) {
		newProject = project;
	}

	@Override
	public String doDefault() throws Exception {
		webResourceManager.requireResourcesForContext("jira-clone-project-plugin");
		setSrc(Long.toString(getSelectedProjectId()));
		return super.doDefault();
	}

	@Override
	protected void doValidation() {
		webResourceManager.requireResourcesForContext("jira-clone-project-plugin");
		super.doValidation();
	}

	@Override
	protected String doExecute() throws Exception {
		webResourceManager.requireResourcesForContext("jira-clone-project-plugin");
		String result = super.doExecute();
		if ("none".equals(result)) {
			Project srcProject = projectManager.getProjectObj(Long.valueOf(getSrc()));

            final List<ProjectSettingsCopierModuleDescriptor> descriptors = pluginAccessor
					.getEnabledModuleDescriptorsByClass(ProjectSettingsCopierModuleDescriptor.class);

			final Comparator<ProjectSettingsCopierModuleDescriptor> compareWeights = new Comparator<ProjectSettingsCopierModuleDescriptor>()
			{
				@Override
				public int compare(ProjectSettingsCopierModuleDescriptor o1, ProjectSettingsCopierModuleDescriptor o2)
				{
					return ComparisonChain.start().compare(o1.getWeight(), o2.getWeight())
							.compare(StringUtils.defaultString(o1.getName()), StringUtils.defaultString(o2.getName()))
							.compare(o1.getCompleteKey(), o2.getCompleteKey()).result();
				}
			};

			for(ProjectSettingsCopierModuleDescriptor descriptor : ImmutableSortedSet
					.copyOf(compareWeights, descriptors)) {
				descriptor.getModule().copySettings(srcProject, newProject, new SimpleErrorCollection());
			}
		}
		return result;
	}
}
